package fr.umfds.tp2_3_6;

public class Calcul {
	
	int add(int a, int b) {
		return a + b;
	}
	
	int sous(int a, int b) {
		return a - b;
	}
	
	int mul(int a, int b) {
		return a * b;
	}
	
	int div(int a, int b) throws DivisionParZeroException {
		if (b == 0) {
			throw new DivisionParZeroException("Division par zero pas possible");
		}
		return a / b;
	}
	
	
	
}
