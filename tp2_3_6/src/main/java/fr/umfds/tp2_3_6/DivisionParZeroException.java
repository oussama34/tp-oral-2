package fr.umfds.tp2_3_6;

public class DivisionParZeroException extends Exception {
	public DivisionParZeroException() {
		super();
	}
	
	public DivisionParZeroException(String s) {
		super(s);
	}
	
}
