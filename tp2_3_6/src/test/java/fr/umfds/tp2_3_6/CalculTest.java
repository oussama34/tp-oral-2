package fr.umfds.tp2_3_6;

import static org.junit.jupiter.api.Assertions.*;

import java.util.concurrent.TimeUnit;

import static org.hamcrest.MatcherAssert.assertThat; 
import static org.hamcrest.Matchers.*;
import static java.time.Duration.*;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

public class CalculTest {
	
	Calcul c;
	
	@BeforeEach
	void setUp() {
		c = new Calcul();
		//System.out.println("Je m'execute avant chaque test");
	}
	
	@Test
	void addQuiMarche() {
		assertEquals(7,c.add(3,4));
	}
	
	@Disabled
	@Test 
	void addAvecPlusieurAssertionsDontUneQuiMarchePas() {
		assertEquals(3,c.add(1, 2));
		assertEquals(20,c.add(10, 10));
		assertEquals(10,c.add(10, 10));
	    assertTrue(5==c.add(3, 2));
	    assertThat(8, equalTo(c.add(3, 5)));
	}
	
	@Disabled
	@Test
	void sousAvecPlusieurAssertionsDontUneQuiMarchePasUtilisantLambda() {
		assertAll("message a afficher en cas d'erreur sur une des assertions suivante:",
				() -> assertEquals(10,c.sous(20, 10)),
				() -> assertTrue(5==c.sous(10, 5)),
				() -> assertEquals(10, c.sous(20, 10)),
				() -> assertFalse(0==c.sous(10, 10), "4eme assertion: 0 est egale a c.sous(10,10)"),
				() -> assertEquals(1, c.sous(101, 100)),
				() -> assertThat(19, equalTo(c.sous(20, 1))));
	}
	
	@Test
	void divInvalideDivisionParZeroException() {
		assertThrows(DivisionParZeroException.class, 
				() -> { c.div(10, 0); });
	}
	
	@Tag("moinsduneseconde")
	@Test
	void addEnMoinsDuneSeconde() {
		assertTimeout(ofSeconds(1), () -> { c.add(3, 3); });
	}
	
	@Test
    @Timeout(value = 100, unit = TimeUnit.MILLISECONDS)
    void failsSiExecutionTimeDepasse100Milliseconds() {
        // fails if execution time exceeds 100 milliseconds
		c.sous(100000, 99999999);
    }
	
	@ParameterizedTest(name = "{0} x 0 doit être égal à 0")
	@ValueSource(ints = { 1, 2, 42, 1011, 5089 })
	public void mulDoitRetournerZeroCarMultiplierParZero(int arg) {
		final int resultatActuel = c.mul(arg, 0);
		assertEquals(0, resultatActuel);
	}

	@AfterEach
	void tearDown() {
		//System.out.println("Je m'execute apres chaque test");
		c = null;
	}
	
}





